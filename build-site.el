(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
;; Install dependencies
(package-install 'htmlize)
(package-install 'julia-ts-mode)

(require 'ox-publish)
(setq org-html-validation-link nil)
(setq org-publish-project-alist
      (list
       (list "jdm204.gitlab.io"
             :recursive t
             :base-directory "./content"
             :publishing-directory "./public"
             :publishing-function 'org-html-publish-to-html
             :html-head (concat
                         "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH\" crossorigin=\"anonymous\">"
                         "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />")
             :html-postamble nil
             :author "jdm204"
             :with-author nil
             :with-creator t
             :with-toc t
             :section-numbers nil
             :time-stamp-file nil)))
(setq org-html-validation-link nil
      org-html-head-include-scripts nil
      org-html-head-include-default-style nil)

(copy-directory "./content/img/" "./public/" nil t)
(copy-directory "./content/CV/" "./public/" nil t)
(copy-directory "./content/molarity/wasm/" "./public/")
(copy-file "./style.css" "./public/" t)
(copy-file "./content/pascal-triangle.pdf" "./public/" t)

(org-publish-all t)

(message "Build complete!")
