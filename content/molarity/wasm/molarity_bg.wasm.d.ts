/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function init_panic_hook(): void;
export function __wbg_reply_free(a: number): void;
export function reply_text(a: number, b: number): void;
export function reply_color(a: number, b: number): void;
export function proc(a: number, b: number): number;
export function main(): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
export function __wbindgen_start(): void;
