use crate::{Mass, MolWeight, Molarity, Volume};

pub enum Parsed {
    Mass(Mass),
    Volume(Volume),
    Concentration(Molarity),
    MolWeight(MolWeight),
}

pub fn parse(s: &str) -> Result<Parsed, String> {
    let split = s.find(char::is_alphabetic).ok_or(format!(
        "I don't understand `{}` (don't forget the units!)",
        s
    ))?;

    let quant = s[..split]
        .parse::<f64>()
        .map_err(|_| format!("I don't understand `{}`, doesn't seem to be a quantity!", s))?;

    match s[split..].to_lowercase().as_str() {
        "m" => Ok(Parsed::Concentration(Molarity::molar(quant))),
        "mm" => Ok(Parsed::Concentration(Molarity::millimolar(quant))),
        "um" => Ok(Parsed::Concentration(Molarity::micromolar(quant))),
        "μm" => Ok(Parsed::Concentration(Molarity::micromolar(quant))),
        "nm" => Ok(Parsed::Concentration(Molarity::nanomolar(quant))),
        "pm" => Ok(Parsed::Concentration(Molarity::picomolar(quant))),

        "kg" => Ok(Parsed::Mass(Mass::kilograms(quant))),
        "g" => Ok(Parsed::Mass(Mass::grams(quant))),
        "mg" => Ok(Parsed::Mass(Mass::milligrams(quant))),
        "ug" => Ok(Parsed::Mass(Mass::micrograms(quant))),
        "μg" => Ok(Parsed::Mass(Mass::micrograms(quant))),
        "ng" => Ok(Parsed::Mass(Mass::nanograms(quant))),
        "pg" => Ok(Parsed::Mass(Mass::picograms(quant))),

        "l" => Ok(Parsed::Volume(Volume::litres(quant))),
        "ml" => Ok(Parsed::Volume(Volume::millilitres(quant))),
        "ul" => Ok(Parsed::Volume(Volume::microlitres(quant))),
        "μl" => Ok(Parsed::Volume(Volume::microlitres(quant))),
        "nl" => Ok(Parsed::Volume(Volume::nanolitres(quant))),
        "pl" => Ok(Parsed::Volume(Volume::picolitres(quant))),

        "g/mol" => Ok(Parsed::MolWeight(MolWeight::gmol(quant))),
        "da" => Ok(Parsed::MolWeight(MolWeight::gmol(quant))),
        "mw" => Ok(Parsed::MolWeight(MolWeight::gmol(quant))),

        otherwise => Err(format!(
            "Error: Don't understand units ({}) for {}",
            otherwise, quant
        )),
    }
}
