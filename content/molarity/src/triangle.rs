use crate::{Mass, MassPerLitre, MolWeight, Molarity, Moles, RecipMolWeight, Volume};

/// Calculate the mass of compound with molecular weight `gmol` to
/// dissolve in a volume `vol` to achieve a concentration `conc`
/// Based on the formula:
/// Mass (g) = Concentration (mol/L) x Volume (L) x Molecular Weight (g/mol)
/// ```
/// use molar::{Volume, Molarity, MolWeight, calc_mass};
/// let mass = calc_mass(Molarity::micromolar(22.0),
///                      Volume::microlitres(77.2),
///                      MolWeight::gmol(442.8));
/// assert_eq!(mass.to_string(), "752.052ng");
/// ```
pub fn calc_mass(conc: Molarity, vol: Volume, gmol: MolWeight) -> Mass {
    (conc * vol) * gmol
}

/// ```
/// use molar::{Mass, Volume, MolWeight, calc_conc};
/// let conc = calc_conc(Mass::milligrams(150.0),
///                      Volume::microlitres(750.0),
///                      MolWeight::gmol(167.0));
/// assert_eq!(conc.to_string(), "1.198M");
/// ```
pub fn calc_conc(mass: Mass, vol: Volume, gmol: MolWeight) -> Molarity {
    (mass / vol) * (1.0 / gmol)
}

/// ```
/// use molar::{Mass, Molarity, MolWeight, calc_vol};
/// let vol = calc_vol(Molarity::millimolar(10.0),
///                    Mass::grams(1.0),
///                    MolWeight::gmol(202.4));
/// assert_eq!(vol.to_string(), "494.071mL");
/// ```
pub fn calc_vol(conc: Molarity, mass: Mass, gmol: MolWeight) -> Volume {
    mass / (conc * gmol)
}

/// ```
/// use molar::{Mass, Volume, Molarity, calc_gmol};
/// let mol_wt = calc_gmol(Molarity::millimolar(100.0),
///                        Volume::millilitres(50.0),
///                        Mass::grams(6.17));
/// assert_eq!(mol_wt.to_string(), "1234.000g/mol");
/// ```
pub fn calc_mw(conc: Molarity, vol: Volume, mass: Mass) -> MolWeight {
    mass / (conc * vol)
}

impl std::ops::Mul<RecipMolWeight> for MassPerLitre {
    type Output = Molarity;

    fn mul(self, rhs: RecipMolWeight) -> Self::Output {
        Molarity::molar(self.raw_grams_per_litre() * rhs.raw_molg())
    }
}

impl std::ops::Div<MolWeight> for f64 {
    type Output = RecipMolWeight;

    fn div(self, rhs: MolWeight) -> Self::Output {
        RecipMolWeight::molg(self / rhs.raw_gmol())
    }
}

impl std::ops::Div<Volume> for Mass {
    type Output = MassPerLitre;

    fn div(self, rhs: Volume) -> Self::Output {
        MassPerLitre::grams_per_litre(self.raw_grams() / rhs.raw_litres())
    }
}

impl std::ops::Div<Moles> for Mass {
    type Output = MolWeight;

    fn div(self, rhs: Moles) -> Self::Output {
        MolWeight::gmol(self.raw_grams() / rhs.raw_moles())
    }
}

impl std::ops::Div<MassPerLitre> for Mass {
    type Output = Volume;

    fn div(self, rhs: MassPerLitre) -> Self::Output {
        Volume::litres(self.raw_grams() / rhs.raw_grams_per_litre())
    }
}

impl std::ops::Mul<Volume> for Molarity {
    type Output = Moles;

    fn mul(self, rhs: Volume) -> Self::Output {
        Moles::moles(self.raw_molar() * rhs.raw_litres())
    }
}

impl std::ops::Mul<MolWeight> for Molarity {
    type Output = MassPerLitre;

    fn mul(self, rhs: MolWeight) -> Self::Output {
        MassPerLitre::grams_per_litre(self.raw_molar() * rhs.raw_gmol())
    }
}

impl std::ops::Mul<MolWeight> for Moles {
    type Output = Mass;

    fn mul(self, rhs: MolWeight) -> Self::Output {
        Mass::grams(self.raw_moles() * rhs.raw_gmol())
    }
}
