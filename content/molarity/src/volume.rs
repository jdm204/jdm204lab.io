use std::f64::EPSILON as E;

/// Volume in kg
#[derive(Clone, Copy, Debug)]
pub struct Volume {
    value: f64,
    exp: f64, // value × 10^exp
}

impl Volume {
    fn new(value: f64, exp: f64) -> Self {
        Volume { value, exp }
    }
    pub fn litres(l: f64) -> Self {
        Volume::new(l, 0.0)
    }
    pub fn millilitres(ml: f64) -> Self {
        Volume::new(ml, -3.0)
    }
    pub fn microlitres(ul: f64) -> Self {
        Volume::new(ul, -6.0)
    }
    pub fn nanolitres(ul: f64) -> Self {
        Volume::new(ul, -9.0)
    }
    pub fn picolitres(ul: f64) -> Self {
        Volume::new(ul, -12.0)
    }
    fn simplify(self) -> Self {
        match self.value {
            x if (1.0..1000.0).contains(&x) => self,
            x if x < 1.0 => Volume::new(x * 1000.0, self.exp - 3.0).simplify(),
            x if x >= 1000.0 => Volume::new(x / 1000.0, self.exp + 3.0).simplify(),
            _ => self,
        }
    }
    pub fn raw_litres(self) -> f64 {
        self.value * 10_f64.powf(self.exp)
    }
}

impl std::fmt::Display for Volume {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Volume { value, exp } = self.simplify();

        let (number, unit) = match exp {
            x if (x - 3.0).abs() < E => (value, "ML"),
            x if (x - 0.0).abs() < E => (value, "L"),
            x if (x - -3.0).abs() < E => (value, "mL"),
            x if (x - -6.0).abs() < E => (value, "μL"),
            x if (x - -9.0).abs() < E => (value, "nL"),
            x if !(-9.0..3.0).contains(&x) => (value * 10_f64.powf(x), "L"),
            _ => panic!("volume exponent not handled:, {}", exp),
        };
        write!(f, "{:.3}{:.3}", number, unit)
    }
}
