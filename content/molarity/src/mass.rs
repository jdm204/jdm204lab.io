use std::f64::EPSILON as E;

/// Mass in kg
#[derive(Clone, Copy, Debug)]
pub struct Mass {
    value: f64,
    exp: f64, // value × 10^exp
}

impl Mass {
    fn new(value: f64, exp: f64) -> Self {
        Mass { value, exp }
    }
    pub fn kilograms(kg: f64) -> Self {
        Mass::new(kg, 0.0)
    }
    pub fn grams(g: f64) -> Self {
        Mass::new(g, -3.0)
    }
    pub fn milligrams(mg: f64) -> Self {
        Mass::new(mg, -6.0)
    }
    pub fn micrograms(ug: f64) -> Self {
        Mass::new(ug, -9.0)
    }
    pub fn nanograms(ng: f64) -> Self {
        Mass::new(ng, -12.0)
    }
    pub fn picograms(pg: f64) -> Self {
        Mass::new(pg, -15.0)
    }
    fn simplify(self) -> Self {
        match self.value {
            x if (1.0..1000.0).contains(&x) => self,
            x if x < 1.0 => Mass::new(x * 1000.0, self.exp - 3.0).simplify(),
            x if x >= 1000.0 => Mass::new(x / 1000.0, self.exp + 3.0).simplify(),
            _ => self,
        }
    }
    pub fn raw_grams(self) -> f64 {
        self.value * 10_f64.powf(self.exp) * 1000.0
    }
}

impl std::fmt::Display for Mass {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Mass { value, exp } = self.simplify();

        let (number, unit) = match exp {
            x if (x - 3.0).abs() < E => (value, "Mg"),
            x if (x - 0.0).abs() < E => (value, "kg"),
            x if (x - -3.0).abs() < E => (value, "g"),
            x if (x - -6.0).abs() < E => (value, "mg"),
            x if (x - -9.0).abs() < E => (value, "μg"),
            x if (x - -12.0).abs() < E => (value, "ng"),
            x if (x - -15.0).abs() < E => (value, "pg"),
            x if !(-15.0..3.0).contains(&x) => (value * 10_f64.powf(x), "kg"),
            x => panic!("mass exponent not multiple of 3: {:.3}", x),
        };
        write!(f, "{:.3}{:.3}", number, unit)
    }
}
