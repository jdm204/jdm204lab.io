/// MolWeight
#[derive(Clone, Copy, Debug)]
pub struct MolWeight {
    gmol: f64,
}

impl MolWeight {
    pub fn gmol(gmol: f64) -> Self {
        MolWeight { gmol }
    }
    pub fn raw_gmol(self) -> f64 {
        self.gmol
    }
}

impl std::fmt::Display for MolWeight {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:.3}g/mol", self.gmol)
    }
}

/// RecipMolWeight
#[derive(Clone, Copy, Debug)]
pub struct RecipMolWeight {
    molg: f64,
}

impl RecipMolWeight {
    pub fn molg(molg: f64) -> Self {
        RecipMolWeight { molg }
    }
    pub fn raw_molg(self) -> f64 {
        self.molg
    }
}

impl std::fmt::Display for RecipMolWeight {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:.3}mol/g", self.molg)
    }
}
