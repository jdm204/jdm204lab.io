use std::f64::EPSILON as E;

#[derive(Clone, Copy, Debug)]
pub struct Moles {
    value: f64,
    exp: f64, // value × 10^exp
}

impl Moles {
    fn new(value: f64, exp: f64) -> Self {
        Moles { value, exp }
    }
    pub fn moles(m: f64) -> Self {
        Moles::new(m, 0.0)
    }
    fn simplify(self) -> Self {
        match self.value {
            x if (1.0..1000.0).contains(&x) => self,
            x if x < 1.0 => Moles::new(x * 1000.0, self.exp - 3.0).simplify(),
            x if x >= 1000.0 => Moles::new(x / 1000.0, self.exp + 3.0).simplify(),
            _ => unreachable!(),
        }
    }
    pub fn raw_moles(self) -> f64 {
        self.value * 10_f64.powf(self.exp)
    }
}

impl std::fmt::Display for Moles {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Moles { value, exp } = self.simplify();

        let (number, unit) = match exp {
            x if (x - 3.0).abs() < E => (value, "kmol"),
            x if (x - 0.0).abs() < E => (value, "mol"),
            x if (x - -3.0).abs() < E => (value, "mmol"),
            x if (x - -6.0).abs() < E => (value, "μmol"),
            x if (x - -9.0).abs() < E => (value, "nmol"),
            x if !(-9.0..3.0).contains(&x) => (value * 10_f64.powf(x), "mol"),
            _ => panic!("moles exponent not reasonable"),
        };
        write!(f, "{:.3}{:.3}", number, unit)
    }
}
