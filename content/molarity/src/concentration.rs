use std::f64::EPSILON as E;

/// Molarity
#[derive(Clone, Copy, Debug)]
pub struct Molarity {
    value: f64,
    exp: f64, // value × 10^exp
}

impl Molarity {
    fn new(value: f64, exp: f64) -> Self {
        Molarity { value, exp }
    }
    pub fn molar(m: f64) -> Self {
        Molarity::new(m, 0.0)
    }
    pub fn millimolar(mm: f64) -> Self {
        Molarity::new(mm, -3.0)
    }
    pub fn micromolar(um: f64) -> Self {
        Molarity::new(um, -6.0)
    }
    pub fn nanomolar(um: f64) -> Self {
        Molarity::new(um, -9.0)
    }
    pub fn picomolar(um: f64) -> Self {
        Molarity::new(um, -12.0)
    }
    fn simplify(self) -> Self {
        match self.value {
            x if (1.0..1000.0).contains(&x) => self,
            x if x < 1.0 => Molarity::new(x * 1000.0, self.exp - 3.0).simplify(),
            x if x >= 1000.0 => Molarity::new(x / 1000.0, self.exp + 3.0).simplify(),
            _ => self,
        }
    }
    pub fn raw_molar(self) -> f64 {
        self.value * 10_f64.powf(self.exp)
    }
}

impl std::fmt::Display for Molarity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Molarity { value, exp } = self.simplify();

        let (number, unit) = match exp {
            x if (x - 0.0).abs() < E => (value, "M"),
            x if (x - -3.0).abs() < E => (value, "mM"),
            x if (x - -6.0).abs() < E => (value, "μM"),
            x if (x - -9.0).abs() < E => (value, "nM"),
            x if !(-9.0..0.0).contains(&x) => (value * 10_f64.powf(x), "M"),
            _ => panic!("molarity exponent not standard: {}", exp),
        };
        write!(f, "{:.3}{:.3}", number, unit)
    }
}

/// MassPerLitre
#[derive(Clone, Copy, Debug)]
pub struct MassPerLitre {
    value: f64,
    exp: f64, // value × 10^exp
}

impl MassPerLitre {
    fn new(value: f64, exp: f64) -> Self {
        MassPerLitre { value, exp }
    }
    pub fn grams_per_litre(gpl: f64) -> Self {
        MassPerLitre::new(gpl, -3.0)
    }
    fn simplify(self) -> Self {
        match self.value {
            x if (1.0..1000.0).contains(&x) => self,
            x if x < 1.0 => MassPerLitre::new(x * 1000.0, self.exp - 3.0).simplify(),
            x if x >= 1000.0 => MassPerLitre::new(x / 1000.0, self.exp + 3.0).simplify(),
            _ => unreachable!(),
        }
    }
    pub fn raw_grams_per_litre(self) -> f64 {
        self.value * 10_f64.powf(self.exp) * 1000.0
    }
}

impl std::fmt::Display for MassPerLitre {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let MassPerLitre { value, exp } = self.simplify();

        let unit = match exp {
            x if (x - 0.0).abs() < E => "kg/L",
            x if (x - -3.0).abs() < E => "g/L",
            x if (x - -6.0).abs() < E => "mg/L",
            x if (x - -9.0).abs() < E => "μg/L",
            _ => panic!("mass/litre exponent not standard"),
        };
        write!(f, "{:.3}{:.3}", value, unit)
    }
}
