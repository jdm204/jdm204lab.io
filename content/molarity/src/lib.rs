// bug: I don't handle display of quants outside of the normal unit ranges

extern crate console_error_panic_hook;

use std::panic;

use wasm_bindgen::prelude::*;

mod amount;
mod concentration;
mod equation;
mod mass;
mod molweight;
mod parse;
mod triangle;
mod volume;

pub use amount::Moles;
pub use concentration::MassPerLitre;
pub use concentration::Molarity;
pub use equation::{equation_string, space_string};
pub use mass::Mass;
pub use molweight::MolWeight;
pub use molweight::RecipMolWeight;
pub use triangle::{calc_conc, calc_mass, calc_mw, calc_vol};
pub use volume::Volume;

use parse::{parse, Parsed};

#[wasm_bindgen]
pub fn init_panic_hook() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}

// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    init_panic_hook();
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    // let window = web_sys::window().expect("no global `window` exists");
    // let document = window.document().expect("should have a document on window");
    // let body = document.body().expect("document should have a body");

    // let echo = document.create_element("p")?;
    // echo.set_inner_html("Type in the box above!");
    // echo.set_id("echo");

    // let cmdline = document.create_element("input")?;
    // cmdline.set_id("cmdline");

    // body.append_child(&echo)?;
    // body.append_child(&cmdline)?;

    Ok(())
}

#[wasm_bindgen]
pub struct Reply {
    text: String,
    color: String,
}

#[wasm_bindgen]
impl Reply {
    pub fn text(&self) -> String {
        self.text.clone()
    }
    pub fn color(&self) -> String {
        self.color.clone()
    }
}

#[wasm_bindgen]
pub fn proc(input: String) -> Reply {
    if input.trim() == "" {
        return Reply {
            text: "Enter quantities into the box above!".to_string(),
            color: "#aeffde".to_string(),
        };
    }
    match Query::from_string(input) {
        Ok(res) => Reply {
            text: res.solve(),
            color: "#dee3ff".to_string(),
        },
        Err(e) => Reply {
            text: e,
            color: "#ffa9a9".to_string(),
        },
    }
}

pub struct Query {
    mass: Option<Mass>,
    conc: Option<Molarity>,
    vol: Option<Volume>,
    mw: Option<MolWeight>,
}

impl Query {
    fn empty() -> Self {
        Query {
            mass: None,
            conc: None,
            vol: None,
            mw: None,
        }
    }
    pub fn from_args() -> Result<Self, String> {
        let mut q = Query::empty();

        for arg in std::env::args().skip(1) {
            match parse(&arg)? {
                Parsed::Mass(m) => q.mass = Some(m),
                Parsed::Concentration(c) => q.conc = Some(c),
                Parsed::Volume(v) => q.vol = Some(v),
                Parsed::MolWeight(w) => q.mw = Some(w),
            }
        }
        Ok(q)
    }
    pub fn from_string(s: String) -> Result<Self, String> {
        let mut q = Query::empty();

        for arg in s.split_whitespace() {
            match parse(arg)? {
                Parsed::Mass(m) => q.mass = Some(m),
                Parsed::Concentration(c) => q.conc = Some(c),
                Parsed::Volume(v) => q.vol = Some(v),
                Parsed::MolWeight(w) => q.mw = Some(w),
            }
        }
        Ok(q)
    }
    pub fn n_unspecified_quantities(&self) -> u8 {
        self.mass.is_none() as u8
            + self.conc.is_none() as u8
            + self.vol.is_none() as u8
            + self.mw.is_none() as u8
    }
    pub fn solve(self) -> String {
        match self {
            // find mass
            Query {
                mass: None,
                conc: Some(conc),
                vol: Some(vol),
                mw: Some(mw),
            } => calc_mass(conc, vol, mw).to_string(),
            // find conc
            Query {
                mass: Some(mass),
                conc: None,
                vol: Some(vol),
                mw: Some(mw),
            } => calc_conc(mass, vol, mw).to_string(),
            // find volume
            Query {
                mass: Some(mass),
                conc: Some(conc),
                vol: None,
                mw: Some(mw),
            } => calc_vol(conc, mass, mw).to_string(),
            // find mw
            Query {
                mass: Some(mass),
                conc: Some(conc),
                vol: Some(vol),
                mw: None,
            } => calc_mw(conc, vol, mass).to_string(),
            _ => match self.n_unspecified_quantities() {
                2 => format!(
                    "The solution is a <strong>curve</strong> in (2D) {} space:<br><ul>{}</ul>",
                    space_string(&self),
                    equation_string(&self),
                ),
                3 => format!(
                    "The solution is a <strong>surface</strong> in (3D) {} space:<br><ul>{}</ul>",
                    space_string(&self),
                    equation_string(&self),
                ),
                _ => "Not a valid query".to_string(),
            },
        }
    }
}
