use crate::Query;

pub fn equation_string(q: &Query) -> String {
    format!(
        "<em>{mass} = {conc} x {vol} x {mw}</em>",
        mass = match q.mass {
            Some(mass) => mass.to_string(),
            None => "mass".to_string(),
        },
        conc = match q.conc {
            Some(conc) => conc.to_string(),
            None => "conc".to_string(),
        },
        vol = match q.vol {
            Some(vol) => vol.to_string(),
            None => "vol".to_string(),
        },
        mw = match q.mw {
            Some(mw) => mw.to_string(),
            None => "mw".to_string(),
        },
    )
}

pub fn space_string(q: &Query) -> String {
    let quants = [
        "<i>mass</i>",
        "<i>concentration</i>",
        "<i>volume</i>",
        "<i>molecular weight</i>",
    ];
    let vars = [
        q.mass.is_none(),
        q.conc.is_none(),
        q.vol.is_none(),
        q.mw.is_none(),
    ];
    let mut space = String::new();

    for (i, quant) in quants.iter().enumerate() {
        if vars[i] {
            if !space.is_empty() {
                space.push_str(" / ");
            }
            space.push_str(quant);
        }
    }
    space
}
