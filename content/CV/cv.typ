#let primary_colour = rgb("#3E0C87") // vivid purple
#let secondary_colour = rgb("#12348e") // blue

#let icon(name, shift: 1.5pt, height: 11pt) = {
    box(
        baseline: shift,
        height: 11pt,
        image("img/" + name)
    )
    h(3pt)
}

#let styled-link(dest, content) = emph(text(
    fill: secondary_colour,
    link(dest, content)
))

#let findMe(services) = {
    set text(8pt)
    services.map(service => {
        styled-link(service.link, service.display)
    }).join(h(10pt))
    [
    
    ]
}

#let term(period) = {
    text(9pt)[#icon("calendar.svg") #period #h(1fr)]
}
#let alta(
    name: "",
    links: (),
    tagline: [],
    content,
) = {
    set document(
        title: name + "'s CV",
        author: name,
    )
    set text(9.8pt, font: "IBM Plex Sans")
    set page(
        margin: (x: 54pt, y: 52pt),
    )

    show heading.where(
        level: 2
    ): it => text(
        fill: primary_colour,
        [
            #{it.body}
            #v(-7pt)
            #line(length: 100%, stroke: 1pt + primary_colour)
        ]
    )
    
    show heading.where(
        level: 3
    ): it => text(it.body)
  
    show heading.where(
        level: 4
    ): it => text(
        fill: primary_colour,
        it.body
    )

    [= #name]

    v(0.8em)
    findMe(links)
    v(1em)
    tagline
    v(1em)

    columns(
        2,
        gutter: 15pt,
        content,
    )
}

#alta(
    name: "Dr Jamie David Matthews",
    links: (
        (name: "email", link: "mailto:jdm204@cam.ac.uk", display: "jdm204@cam.ac.uk"),
        (name: "website", link: "https://jdm204.gitlab.io/", display: "jdm204.gitlab.io"),
        (name: "gitlab", link: "https://gitlab.com/jdm204", display: "jdm204 on GitLab"),
    ),
    tagline: [Postdoctoral researcher in bioinformatics and cancer biology, interested in basic and translational research with a focus on computation. Proponent of collaborative and open science. Currently employed as a _Research Associate_ in Prof. Suzanne Turner's group in Cambridge, looking for a computational role.],
    [

        == Education

        #grid(columns: (1.5fr, 1fr), rows: (24pt), box[
            === PhD Pathology \
            #term[Oct 2017 --- Feb 2022]
        ], box(height: 18pt)[#image("img/cam_logo.jpg")])
     
        My project involved developing patient-derived xenograft (PDX) models from rare primary paediatric patient lymphoma samples to evaluate drug resistance and treatment options in relapse and refractory disease, investigating persister cell populations, and inferring clonal evolution and genetic intratumour heterogeneity from NGS datasets. Supervised by Prof Suzanne D Turner. Funded by the #styled-link("https://alexhulmefoundation.co.uk/")[Alex Hulme Foundation].

        #line(length: 100%, stroke: 0.4pt + secondary_colour)

        #grid(columns: (1.8fr, 1fr), rows: (24pt), box[
            === MSc Integrated Immunology \
            #term[Oct 2016 --- Aug 2017]
        ], image("img/ox_logo.svg"))
    
        My MSc project spanned across oncology, immunology and bioinformatics labs, which sparked interests in cancer and computational biology. I performed confocal microscopy of FFPE triple-negative breast cancer and stimulated primary T cell subsets _in vitro_ to compare cytokine secretion between normoxia and hypoxia.
    
        #line(length: 100%, stroke: 0.4pt + secondary_colour)
        #grid(columns: (1.5fr, 1fr), rows: (24pt), box[
            === BSc Biology \
            #term[Sep 2012 --- Aug 2015]
        ], box(height: 32pt)[#image("img/qm_logo.png")])

        I cloned and expressed a human HIV-1 restriction factor in _E coli_, to help understand the basic biology of intrinsic immunity to retroviruses. I graduated with *First Class Honours* in both academic and practical work and received the *Drapers’ Company Prize for Outstanding Achievement*.
  
        #v(1em)
      
        == Skills and Experience

        === Laboratory
      
        - *in vivo PDX models*: patient sample engraftment, passage, drug challenge, humanisation
        - *Sequencing*: DNA/RNA extraction/quantification, Nanopore library preparation/sequencing
        - *Cell culture*: adherent/suspension, mouse/human cell lines, primary co-culture
        - *Flow cytometry*: phenotyping, intracellular staining, cell cycle analysis, cell sorting
        - *Imaging*: multicolour immunofluorescence, confocal microscopy
        - *Liquid handling robot*: high-throughput drug screening

        === Computational

        - *Somatic cancer genomics*: SNV, indel, structural variant calling
        - *Transcriptomics*: bulk/single cell RNA preprocessing, Seurat/Scanpy
        - *Reproducible analysis pipelines*: Snakemake, Conda, Docker, Apptainer/Singularity, git, nix
        - *Data analysis and visualisation*: R, Julia, Bayesian inference
        - *Programming/HPC*: Rust, Bash, SLURM, Python
  
        === Communication

        - *Graphics and design*: Markdown, LaTeX, Typst, Pandoc, Inkscape, `ggplot2`
        - *Web*: HTML5, webassembley
      
        == Teaching and Administration

        - As manager of the mouse colony during my PhD, I arranged purchasing, importing, breeding and health monitoring in conjunction with animal staff, as well as teaching animal handling and experimental procedures to lab members
        - During a collaborative project with the #styled-link("https://www.uci.or.ug/")[Uganda Cancer Institute], I trained a visiting master’s student in several lab techniques including PCR and flow cytometry
        - supervised a computational undergraduate project during the COVID-19 pandemic, for which the student constructed a Snakemake pipeline to analyse publicly-available cancer sequencing data
        - assisted students during computational practical sessions as a demonstrator for Parts 1a and 1b Mathematical Biology at the University of Cambridge
        - designed, supervised, and assessed a laboratory-based undergraduate project

        == Selected Publications

        *Targeting NRAS via miR-1304-5p or farnesyltransferase inhibition confers sensitivity to ALK inhibitors in ALK-mutant neuroblastoma*

        Nature Communications, 2024

        #text(size: 0.8em)[_Perla Pucci, Liam C. Lee, Miaojun Han, *Jamie D. Matthews*, Leila Jahangiri, Michaela Schlederer, Eleanor Manners, Annabel Sorby-Adams, Joshua Kaggie, Ricky M. Trigg, Christopher Steel, Lucy Hare, Emily R. James, Nina Prokoph, Stephen P. Ducray, Olaf Merkel, Firkret Rifatbegovic, Ji Luo, Sabine Taschner-Mandl, Lukas Kenner, G. A. Amos Burke, Suzanne D. Turner_]

        *STAT3 couples activated tyrosine kinase signaling to the oncogenic core transcriptional regulatory circuitry of anaplastic large cell lymphoma*

        Cell Reports Medicine, 2024
      
        #text(size: 0.8em)[_Nicole Prutsch, Shuning He, Alla Berezovskaya, Adam D. Durbin, Neekesh V. Dharia, Kelsey A. Maher, *Jamie D. Matthews*, Lucy Hare, Suzanne D. Turner, Kimberly Stegmaier, Lukas Kenner, Olaf Merkel, A. Thomas Look, Brian J. Abraham, Mark W. Zimmerman_]

        #v(1em)

        *Patient-derived xenograft models of ALK+ ALCL reveal preclinical promise for therapy with brigatinib*

        British Journal of Haematology, 2023

        #text(size: 0.8em)[_Nina Prokoph, *Jamie D. Matthews*_ (co-first)_, Ricky M. Trigg, Ivonne A. Montes-Mojarro, G. A. Amos Burke, Falko Fend, Olaf Merkel, Lukas Kenner, Birgit Geoerger, Robert Johnston, Matthew J. Murray, Charlotte Riguad, Laurence Brugières, Suzanne D. Turner_]

        #v(1em)
        *Sequential inverse dysregulation of the RNA helicases DDX3X and DDX3Y facilitates MYC-driven lymphomagenesis*

        Molecular Cell, 2021
      
        #text(size: 0.8em)[_Chun Gong, Joanna A Krupka, Jie Gao, Nicholas F Grigoropoulos, George Giotopoulos, Ryan Asby, Michael Screen, Zelvera Usheva, Francesco Cucco, Sharon Barrans, Daniel Painter, Nurmahirah Binte Mohammed Zaini, Björn Haupl, Susanne Bornelöv, Igor Ruiz De Los Mozos, Wei Meng, Peixun Zhou, Alex E Blain, Sorcha Forde, *Jamie Matthews*, Michelle Guet Khim Tan, G A Amos Burke, Siu Kwan Sze, Philip Beer, Cathy Burton, Peter Campbell, Vikki Rand, Suzanne D Turner, Jernej Ule, Eve Roman, Reuben Tooze, Thomas Oellerich, Brian J Huntly, Martin Turner, Ming-Qing Du, Shamith A Samarajiwa, Daniel J Hodson_]

        #v(1em)

        *Paediatric Burkitt lymphoma patient-derived xenografts capture disease characteristics over time and are a model for therapy.*
      
        British Journal of Haematology (2020)
      
        #text(size: 0.8em)[_Sorcha Forde, *Jamie D. Matthews*, Leila Jahangiri, Liam C. Lee, Nina Prokoph, Tim I.M. Malcolm, Olivier T. Giger, Natalie Bell, Helen Blair, Aengus O'Marcaigh, Owen Smith, Lukas Kenner, Simon Bomken, Gladstone A. A. Burke, Suzanne D. Turner_]

        #v(1em)

        *IL10RA Modulates Crizotinib Sensitivity in NPM1-ALK-positive Anaplastic Large Cell Lymphoma*
      
        Blood (2020) 136 (14): 1657–1669.
      
        #text(size: 0.8em)[_Nina Prokoph, Nicola A. Probst, Liam C. Lee, Jack M. Monahan, *Jamie D. Matthews*, Huan-Chang Liang, Klaas Bahnsen, Ivonne A. Montes-Mojarro, Elif Karaca-Atabay, Geeta G. Sharma, Vikas Malik, Hugo Larose, Sorcha D. Forde, Stephen P. Ducray, Cosimo Lobello, Qi Wang, Shi-Lu Luan, Šárka Pospíšilová, Carlo Gambacorti-Passerini, G. A. Amos Burke, Shahid Pervez, Andishe Attarbaschi, Andrea Janíková, Hélène Pacquement, Judith Landman-Parker, Anne Lambilliotte, Gudrun Schleiermacher, Wolfram Klapper, Ralf Jauch, Wilhelm Woessmann, Gilles Vassal, Lukas Kenner, Olaf Merkel, Luca Mologni, Roberto Chiarle, Laurence Brugières, Birgit Geoerger, Isaia Barbieri, Suzanne D. Turner_]

        #v(1em)
        
        *Whole Exome Sequencing reveals NOTCH1 mutations in anaplastic large cell lymphoma and points to Notch both as a key pathway and a potential therapeutic target*
      
        Haematologica Early view Apr 23, 2020

        #text(size: 0.8em)[_Hugo Larose, Nina Prokoph, *Jamie D. Matthews*, Michaela Schlederer, Sandra Högler, Ali F. Alsulami, Stephen P. Ducray, Edem Nuglozeh, Feroze M.S. Fazaludeen, Ahmed Elmouna, Monica Ceccon, Luca Mologni, Carlo Gambacorti-Passerini, Gerald Hoefler, Cosimo Lobello, Sarka Pospisilova, Andrea Janikova, Wilhelm Woessmann, Christine Damm-Welk, Martin Zimmermann, Alina Federova, Andrea Malone, Owen Smith, Mariusz Wasik, Giorgio Inghirami, Laurence Lamant, Tom L. Blundell, Wolfram Klapper, Olaf Merkel, G.A. Amos Burke, Shahid Mian, Ibraheem Ashankyty, Lukas Kenner, Suzanne D. Turner_]

        #v(1em)
      
        *The targetable kinase PIM1 drives ALK inhibitor resistance in high-risk neuroblastoma independent of MYCN status*
      
        Nature Communications volume 10, Article number: 5428 (2019)
      
        #text(size: 0.8em)[_Ricky M. Trigg, Liam C. Lee, Nina Prokoph, Leila Jahangiri, C. Patrick Reynolds, G. A. Amos Burke, Nicola A. Probst, Miaojun Han, *Jamie D. Matthews*, Hong Kai Lim, Eleanor Manners, Sonia Martinez, Joaquin Pastor, Carmen Blanco-Aparicio, Olaf Merkel, Ines Garces de los Fayos Alonso, Petra Kodajova, Simone Tangermann, Sandra Högler, Ji Luo, Lukas Kenner & Suzanne D. Turner_]

        #v(1em)

        == Book Chapters

        - *Establishing Patient-Derived Xenograft (PDX) Models of Lymphoma*: Methods in Molecular Biology: Lymphoma, Springer Publishing, 2024 (_in print_)
        
        == Seminars

        - Invited talk: *Brigatinib Shows Preclinical Promise for Aggressive Relapsed/Refractory ALK-Positive ALCL*, _The 7th International Symposium on Childhood, Adolescent & Young Adult Non-Hodgkin Lymphoma_, New York, USA (2022)
      
        == Open Source

        #grid(columns: (0.5fr, 1.5fr), box[#image("img/tidyvcf.png", width: 85%)], box[
            #styled-link("https://gitlab.com/jdm204/tidyvcf")[*tidyvcf* :]
            a simple command-line utility to convert VCF files into tidy tables. Written in Rust.
        ])

        #line(length: 100%, stroke: 0.4pt + secondary_colour)

        #grid(columns: (0.5fr, 1.5fr), box(inset: (left: 5pt))[#image("img/wellplates.png", width: 70%)], box[
            #styled-link("https://gitlab.com/jdm204/wellplates.jl")[*WellPlates.jl* :]
            a Julia package to assist with working with multiwell plate experiments
        ])

        // #line(length: 100%, stroke: 0.4pt + secondary_colour)
        // #styled-link("https://gitlab.com/jdm204/wellplates.jl")[*bulk seq snakemake* :]
        // a reference pipeline for variant calling in cancer DNA/RNA sequencing
    ],
)
